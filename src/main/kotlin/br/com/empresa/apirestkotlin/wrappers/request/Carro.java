package br.com.empresa.apirestkotlin.wrappers.request;

import java.util.Objects;

public class Carro {

    private Integer ano;
    private String marca;

    public Carro() {
    }

    public Carro(Integer ano, String marca) {
        this.ano = ano;
        this.marca = marca;
    }

    public Carro(Integer ano) {
        this.ano = ano;
    }

    public Carro(String marca) {
        this.marca = marca;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Carro carro = (Carro) o;
        return Objects.equals(ano, carro.ano) && Objects.equals(marca, carro.marca);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ano, marca);
    }

    @Override
    public String toString() {
        return "Carro{" +
                "ano=" + ano +
                ", marca='" + marca + '\'' +
                '}';
    }
}