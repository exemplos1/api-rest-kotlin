package br.com.empresa.apirestkotlin.controller

import br.com.empresa.apirestkotlin.service.ModuloService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("modulo")
class ModuloController(private val moduloService: ModuloService) {

    @GetMapping("{num}")
    fun calculo(@PathVariable num: Int): Int {
        return moduloService.modulo(num)
    }

    @GetMapping("{num}/{mod}")
    fun calculo(@PathVariable num: Int, @PathVariable mod: Int): Int {
        return moduloService.modulo(num, mod)
    }

}